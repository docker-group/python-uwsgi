FROM python:3.7

COPY ./rc.local /etc/rc.local
RUN chmod +x /etc/rc.local
COPY . /bd_build

RUN /bd_build/prepare.sh && \
	/bd_build/system_services.sh && \
	/bd_build/utilities.sh && \
	/bd_build/cleanup.sh

RUN rm -rf /bd_build

ENV DEBIAN_FRONTEND="teletype" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8"

RUN pip install pip --upgrade && mkdir /var/www/ && apt-get update && apt-get install -y libxml2 git nano less wait-for-it gettext && pip install awscli --upgrade && pip install botocore --upgrade && pip install uwsgi
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/sbin/my_init"]
